package errors

import (
	"errors"
	"fmt"
	"runtime"
	"strconv"
)

type Error struct {
	cause error
	file  string
	line  int
}

func New(message string) error {
	err := Error{cause: errors.New(message)}
	err.Locate()
	return err
}

func (e Error) Cause() error {
	return e.cause
}

func (e Error) Error() string {
	return e.cause.Error()
}

func Trace(err error) error {
	if err == nil {
		return nil
	}
	newErr := Error{cause: err}
	newErr.Locate()
	return newErr
}

func (e Error) Format(f fmt.State, v rune) {
	switch v {
	case 'v':
		f.Write([]byte(e.file + ":" + strconv.Itoa(e.line) + " " + e.cause.Error()))
	case 's':
		f.Write([]byte(e.cause.Error()))
	}
}

func (e *Error) Locate() {
	_, e.file, e.line, _ = runtime.Caller(2)
}
